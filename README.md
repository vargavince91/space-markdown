This is a [link](File.md) to a file without spaces. It works. Yay.

However, I can't create a link to a filename that contains spaces.

* [`[Link](File with spaces.md)`](File with spaces.md)
* [`[Link](File%20with%20spaces.md)`](File%20with%20spaces.md)
* [`[Link](File\ with\ spaces.md)`](File\ with\ spaces.md)
* [`[Link]("File with spaces.md")`]("File with spaces.md")

For more information see my question on [Stack Overflow](http://stackoverflow.com/questions/34569256/link-to-filenames-with-spaces-in-bitbucket-markdown)

* [`[Link](File-with-spaces.md)`](File-with-spaces.md)
* [`[Link](vargavince91/space-markdown/File-with-spaces.md)`](vargavince91/space-markdown/File-with-spaces.md)
* [`[Link](File-with-spaces.md-)`](File-with-spaces.md-)
* [`[Link](vargavince91/space-markdown/File-with-spaces.md-)`](vargavince91/space-markdown/File-with-spaces.md-)
